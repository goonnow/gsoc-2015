# SummerOfCode2015
## Projects AutoConfigWebApps ( WarmUpTasks )
ref: https://wiki.debian.org/SummerOfCode2015/Projects/AutoConfigWebApps/WarmUpTasks

### Bootstrap the project
`vagrant up && vagrant provision`

### Task 1:  ( ./my-recipes/cookbooks/task1 )
recipe for installing **vim** and **bash-completion** 

**How to test**

1. `vagrant ssh`

2. `dpkg-query  -l | grep 'vim\|bash`

### Task 2: ( ./my-recipes/cookbooks/nginx )
Set up nginx

**How to test**

1. The site is served via `/srv/my-site`

2. The site's configuration is `/etc/nginx/conf.d/mysite.conf`

3. This url ( `my.site:8080` ) should be accessible from your machine.