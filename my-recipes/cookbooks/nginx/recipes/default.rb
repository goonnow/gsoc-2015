#
# Cookbook Name:: nginx
# Recipe:: default
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

package 'nginx' do
  action :install
end

service 'nginx' do
  action [ :enable, :start ]
end

cookbook_file "/etc/nginx/conf.d/my-site.conf" do
   source "my-site.conf"
   action :create
   notifies :restart, "service[nginx]"
end
